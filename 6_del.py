def coroutine(func):
    def inner(*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g

    return inner


class CustomException(Exception):
    pass


def subgen():
    while True:
        try:
            message = yield
        except StopIteration:
            print('StopIteration')
            break
        else:
            print('...', message)
    return 'returned from subgen()'

@coroutine
def delegator(g):
    # while True:
    #     try:
    #         data = yield
    #         g.send(data)
    #     except:
    #         pass
    result = yield from g
    print(result)
